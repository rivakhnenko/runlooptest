package com.runloop.test.ui.main.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableField
import com.runloop.test.ui.main.api.RssFactory
import com.runloop.test.ui.main.api.RssService
import me.toptas.rssconverter.RssFeed
import me.toptas.rssconverter.RssItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.TimeUnit
import kotlin.concurrent.fixedRateTimer
import kotlin.concurrent.thread


/**
 * Created by Ruslan Ivakhnenko on 26.08.2019.
 *
 * e-mail: ruslan1910@gmail.com
 */
class RssRepositoryImpl(val isLoading : ObservableField<Boolean>) : RssRepository {

    private val service = RssFactory().getRetrofit()

    private val defaultPeriod = TimeUnit.SECONDS.toMillis(5)

    override fun getNews() : LiveData<RssFeed> {
        val liveData = MutableLiveData<RssFeed>()

        fixedRateTimer(period = defaultPeriod) {
            isLoading.set(true)
            service.getNews().enqueue(object : Callback<RssFeed> {

                override fun onFailure(call: Call<RssFeed>, t: Throwable) {
                    liveData.value = null
                    isLoading.set(false)
                }

                override fun onResponse(call: Call<RssFeed>, response: Response<RssFeed>) {
                    liveData.value = if (response.isSuccessful) response.body() else null
                    isLoading.set(false)
                }

            })
        }

        return liveData
    }

    override fun collectOtherNews(): LiveData<List<RssItem>> {
        val mutableLiveData = MutableLiveData<List<RssItem>>()
        fixedRateTimer(period = defaultPeriod) {
            val feedResultList = arrayListOf<RssItem>()
            val entertainmentResponse = service.getEntertainment().execute()
            val environmentResponse = service.getEnvironment().execute()
            if (entertainmentResponse.isSuccessful) {
                feedResultList.addAll(entertainmentResponse.body()?.items ?: arrayListOf())
            }
            if (environmentResponse.isSuccessful) {
                feedResultList.addAll(environmentResponse.body()?.items ?: arrayListOf())
            }

            mutableLiveData.postValue(feedResultList)
        }

        return mutableLiveData
    }

    override fun getEntertainment(): LiveData<RssFeed> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getEnvironment(): LiveData<RssFeed> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}