package com.runloop.test.ui.main.fragments


import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.runloop.test.MainActivity

import com.runloop.test.R
import com.runloop.test.databinding.FragmentFeedDetailBinding
import me.toptas.rssconverter.RssItem

/**
 * A simple [Fragment] subclass.
 */
class FeedDetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val feedItem = activity?.intent?.getSerializableExtra(MainActivity.ARG_FEED_ITEM) as RssItem
        val binding = DataBindingUtil.inflate<FragmentFeedDetailBinding>(inflater, R.layout.fragment_feed_detail, container, false)
        activity?.title = feedItem.title
        binding.feedDetail = feedItem
        return binding.root
    }


}
