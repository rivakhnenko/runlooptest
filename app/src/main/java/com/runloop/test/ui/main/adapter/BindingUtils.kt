package com.runloop.test.ui.main.adapter

import android.databinding.BindingAdapter
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.TextView
import com.runloop.test.R
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat


/**
 * Created by Ruslan Ivakhnenko on 26.08.2019.
 *
 * e-mail: ruslan1910@gmail.com
 */
class BindingUtils {

    companion object {
        @BindingAdapter("imageUrl")
        @JvmStatic
        fun setImageUrl(imageView : ImageView, url : String?) {
            url?.apply { Picasso.get().load(this).error(R.drawable.ic_android).into(imageView) }

        }

        @BindingAdapter("setDate")
        @JvmStatic
        fun setDate(textView : TextView, inputDate: String) {
            //Mon, 26 Aug 2019 11:27:32 -0400
            val dateFormat = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss X")
            val date = dateFormat.parse(inputDate)
            dateFormat.applyPattern("dd.MM.yyyy HH:mm")
            textView.text = dateFormat.format(date)
        }

        @BindingAdapter("setWebContent")
        @JvmStatic
        fun setWebContent(webView : WebView, url: String) {
            webView.webViewClient = WebViewClient()
            webView.settings.apply {
                javaScriptEnabled = true
                mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
//                loadsImagesAutomatically = true
            }
            webView.loadUrl(url)
        }
    }

}