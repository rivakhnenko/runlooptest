package com.runloop.test.ui.main.fragments

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import com.runloop.test.ui.main.viewmodel.MainViewModel


/**
 * Created by Ruslan Ivakhnenko on 27.08.2019.
 *
 * e-mail: ruslan1910@gmail.com
 */
open class BaseFragment : Fragment() {

    lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!).get(MainViewModel::class.java)
    }
}