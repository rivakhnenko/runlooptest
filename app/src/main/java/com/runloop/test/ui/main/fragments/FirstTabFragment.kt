package com.runloop.test.ui.main.fragments

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.runloop.test.R
import kotlinx.android.synthetic.main.first_tab_fragment.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.concurrent.fixedRateTimer

class FirstTabFragment : BaseFragment() {

    private val dateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm:ss")
    private lateinit var fixedRateTimer : Timer

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.first_tab_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fixedRateTimer = fixedRateTimer(period = TimeUnit.SECONDS.toMillis(1)) {
            isResumed?.run { tvCurrentDate.post { tvCurrentDate.text = getCurrentDateTime() } }
        }


        viewModel.selectedFeed.observe(this, Observer {
            tvLabel.text = "Latest selected Feed\n ${it?.title}"
        })

    }

    override fun onDestroyView() {
        super.onDestroyView()
        fixedRateTimer.cancel()
    }

    private fun getCurrentDateTime() = dateFormat.format(Date())


}
