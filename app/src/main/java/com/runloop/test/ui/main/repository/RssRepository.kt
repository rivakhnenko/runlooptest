package com.runloop.test.ui.main.repository

import android.arch.lifecycle.LiveData
import me.toptas.rssconverter.RssFeed
import me.toptas.rssconverter.RssItem


/**
 * Created by Ruslan Ivakhnenko on 26.08.2019.
 *
 * e-mail: ruslan1910@gmail.com
 */
interface RssRepository {

    fun getNews() : LiveData<RssFeed>

    fun getEntertainment() : LiveData<RssFeed>

    fun getEnvironment() : LiveData<RssFeed>

    fun collectOtherNews() : LiveData<List<RssItem>>
}