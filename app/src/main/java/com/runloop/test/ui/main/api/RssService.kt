package com.runloop.test.ui.main.api

import android.arch.lifecycle.LiveData
import me.toptas.rssconverter.RssFeed
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url


/**
 * Created by Ruslan Ivakhnenko on 26.08.2019.
 *
 * e-mail: ruslan1910@gmail.com
 */
interface RssService {

    @GET("businessNews")
    fun getNews() : Call<RssFeed>

    @GET("entertainment")
    fun getEntertainment() : Call<RssFeed>

    @GET("environment")
    fun getEnvironment() : Call<RssFeed>
}