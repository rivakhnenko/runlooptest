package com.runloop.test.ui.main.fragments

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.runloop.test.BR
import com.runloop.test.R
import com.runloop.test.ui.main.adapter.BaseRecycleViewAdapter
import kotlinx.android.synthetic.main.feeds_tab_fragment.*
import me.toptas.rssconverter.RssFeed
import me.toptas.rssconverter.RssItem

class FeedsTabFragment : BaseFragment() {

    lateinit var newsAdapter : BaseRecycleViewAdapter<RssItem>

    lateinit var collectedAdapter : BaseRecycleViewAdapter<RssItem>


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.feeds_tab_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getNews().observe(this, Observer<RssFeed> {
            val list = it?.items ?: arrayListOf()
            if (::newsAdapter.isInitialized){
                newsAdapter.setData(list)
            } else {
                newsAdapter = BaseRecycleViewAdapter(list , BR.feed, R.layout.item_feed) {
                    viewModel.selectedFeed.value = it
                }
                newsRecycleView.adapter = newsAdapter
            }

        })

        viewModel.collectOtherNews().observe(this, Observer<List<RssItem>> {
            if (::collectedAdapter.isInitialized) {
                collectedAdapter.setData(it ?: arrayListOf())
            } else {
                collectedAdapter = BaseRecycleViewAdapter(it ?: arrayListOf(), BR.feed, R.layout.item_feed) {
                    viewModel.selectedFeed.value = it
                }
                entertainmentRecycleView.adapter = collectedAdapter
            }
        })
    }

}
