package com.runloop.test.ui.main.api

import me.toptas.rssconverter.RssConverterFactory
import retrofit2.Retrofit


/**
 * Created by Ruslan Ivakhnenko on 26.08.2019.
 *
 * e-mail: ruslan1910@gmail.com
 */
class RssFactory {

    fun getRetrofit() : RssService {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://feeds.reuters.com/reuters/")
//            .addCallAdapterFactory(LiveDataCallAdapterFactory)
            .addConverterFactory(RssConverterFactory.create()).build()
        return retrofit.create(RssService::class.java)
    }
}