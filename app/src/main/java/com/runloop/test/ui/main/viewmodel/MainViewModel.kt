package com.runloop.test.ui.main.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableField
import com.runloop.test.ui.main.repository.RssRepositoryImpl
import me.toptas.rssconverter.RssItem

class MainViewModel(app : Application) : AndroidViewModel(app) {

    var isLoading : ObservableField<Boolean> = ObservableField()

    private val repository = RssRepositoryImpl(isLoading)

    var selectedFeed : MutableLiveData<RssItem> = MutableLiveData()

    fun getNews() = repository.getNews()

    fun getEntertainment() = repository.getEntertainment()

    fun getEnvironment() = repository.getEnvironment()

    fun collectOtherNews() = repository.collectOtherNews()
}
